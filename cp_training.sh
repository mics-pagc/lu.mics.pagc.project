#!/bin/bash -l
# Time-stamp: <Mon 2018-06-18 09:48 svarrette>
##################################################################

##########################
#                        #
#  The SLURM directives  #
#                        #
##########################
#
#          Set number of resources
#
#SBATCH -N 1
#SBATCH --ntasks-per-node=2
### -c, --cpus-per-task=<ncpus>
###     (multithreading) Request that ncpus be allocated per process
#SBATCH -c 1
#
#SBATCH --time=0-04:00:00   # 1 hour
#
#          Set the name of the job
###SBATCH -J NAME

#          Passive jobs specifications
#SBATCH --partition=interactive
#SBATCH --qos qos-interactive

### General SLURM Parameters
echo "SLURM_JOBID  = ${SLURM_JOBID}"
echo "SLURM_JOB_NODELIST = ${SLURM_JOB_NODELIST}"
echo "SLURM_NNODES = ${SLURM_NNODES}"
echo "SLURM_NTASKS = ${SLURM_NTASKS}"
echo "SLURMTMPDIR  = ${SLURMTMPDIR}"
echo "Submission directory = ${SLURM_SUBMIT_DIR}"

### Guess the run directory
# - either the script directory upon interactive jobs
# - OR the submission directory upon passive/batch jobs
SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
if [ -n "${SLURM_SUBMIT_DIR}" ]; then
    [[ "${SCRIPTDIR}" == *"slurmd"* ]] && RUNDIR=${SLURM_SUBMIT_DIR} || RUNDIR=${SCRIPTDIR}
else
    RUNDIR=${SCRIPTDIR}
fi

### Prepare log file



### Toolbox function
print_error_and_exit() { echo "***ERROR*** $*"; exit 1; }

############################################
################# Let's go #################
############################################

# Use the RESIF build modules
if [ -f  /etc/profile ]; then
   .  /etc/profile
fi
SCRATCH_DIR="/scratch/users/$USER"

# Directory holding your built applications
APPDIR="$HOME"
DEST="${SCRATCH_DIR}/train"
# The task to be executed i.E. your favorite Java/C/C++/Ruby/Perl/Python/R/whatever program
# to be invoked in parallel

# The command to run
### General MPI Case:
# CMD="srun -n $SLURM_NTASKS ${TASK}"
### OpenMPI case if you wish to specialize the MCA parameters
#CMD="mpirun -np $SLURM_NTASKS --mca btl openib,self,sm ${TASK}"

### Prepare logfile
LOGFILE="${RUNDIR}/$(date +%Y-%m-%d)_$(basename ${TASK})_${SLURM_JOBID}.log"

cat > ${LOGFILE} <<EOF
# Task ${TASK} run # MPI @ $(date) by:
#      ${CMD}
#
# SLURM_JOBID        $SLURM_JOBID
# SLURM_JOB_NODELIST $SLURM_JOB_NODELIST
# SLURM_NNODES       $SLURM_NNODES
# SLURM_NTASKS       $SLURM_NTASKS
# SLURM_SUBMIT_DIR   $SLURM_SUBMIT_DIR
### Starting timestamp: $(date +%s)
EOF

# Run the command
if [ -d ${DEST} ];
then
	:
else
	mkdir "${DEST}"
fi
for folder in $(ls /work/projects/bigdata_sets/ImageNet/ILSVRC2012/raw-data/train/ | head -n 600)
do
        if [ -d ${SCRATCH}/train/$folder ] ;
        then
                :
        else
                cp -rf /work/projects/bigdata_sets/ImageNet/ILSVRC2012/raw-data/train/$folder ${SCRATCH}/train
        fi
done

cat >> ${LOGFILE} <<EOF
### Ending timestamp:     $(date +%s)
EOF
