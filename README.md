# Folders organisation
- `runs/{pytorch,horovod}` contains raw logs files of the different runs per configuration
- `scripts/{pytorch,horovod}` contains scripts used to schedule jobs, extract data, and train the model
- `cp_{validation,train}.sh` was used to copy validation and train data to corresponding scratch folder
- `move.sh` used to reduce dataset size
# CNN architectures
 - ResNet-50
# Reference framework
- Horovod + TensorFlow
- PyTorch native
# Hardware configuration
- GPU accelerated (1 to 8 Nodes)
    - -N x - G y
# Metrics
- Image/sec
- Time to reach a certain accuracy
- Epochs needed to reach a certain accuracy
- Accuracy reached in a certain amount of time 

# IMAGENET organisation
Each folder names represent a `wnid` which is an id composed of a concatenation of differents ids based on the [wordnet classification](https://wordnet.princeton.edu/).
The noun correspondance of all wnid of image net can be found in the `word.txt` file.
Each folders thus correspond to a category and all the files under this folder are sharing the same category.
As wordnet is based on synonymes some word might share multiple ids correspondance can be found in the `wordnet.is_a.txt` file.


