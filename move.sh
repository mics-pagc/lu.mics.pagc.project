BASEDIR=$SCRATCH
FOLDER=train
#FOLDER=val
WORKDIR=$SCRATCH/imagenet/$FOLDER
TMPDIR=$WORKDIR/tmp
echo "working in dir $WORKDIR"
if [ -d $TMPDIR ]
then
	rm -r $TMPDIR
else
	mkdir $TMPDIR
fi

echo "copying files"
for file in $(ls $WORKDIR | head -n 100);
do
	mv $WORKDIR/$file $TMPDIR
done
echo "removing excess"
rm -rf $WORKDIR/n*
echo "moving needed files"
mv $TMPDIR/* $WORKDIR
echo "cleaning up"
rm -rf $TMPDIR
