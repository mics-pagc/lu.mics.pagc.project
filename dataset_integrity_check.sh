SCRATCHDIR="/scratch/users/$USER/train"
echo checking $SCRATCHDIR
EXPECTEDFILES=1300
errcnt=0
for f in  $(ls -d $SCRATCHDIR/n*)
do
filename=${f##*/}
origfile="/work/projects/bigdata_sets/ImageNet/ILSVRC2012/raw-data/train/$filename"
origfilecount=$(ls $origfile | wc -l)
filecount=$(echo $(ls $f | wc -l))
	if [ $filecount -eq $origfilecount ]
	then
		:
	else
		echo $f does not contain $origfilecount files
		((errcnt++))
	fi
done
if [  $errcnt -eq 0 ]
then
	echo INTEGRITY CHECK OK
else
	echo FILECOUNT MISSMATCH IN ABOVE DIRS... 
fi
