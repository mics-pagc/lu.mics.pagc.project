#!/bin/bash -l
#SBATCH -J MHVTFG-H2N1G2
#SBATCH -o %x_%j.out
#SBATCH -N 1
#SBATCH --ntasks-per-node=2
#SBATCH --gpus-per-node=2
#SBATCH -t 02:00:0
#SBATCH -p gpu
#SBATCH --reservation=parallelcomputinggpu

## Only needed during HPC School 2019.06
module load swenv/default-env/v1.2-20191021-production
#module load swenv/default-env/devel

## Part of 2019 software environment
module load vis/Pillow/6.0.0-GCCcore-8.2.0
module load lib/TensorFlow/1.13.1-foss-2019a-Python-3.7.2
module load math/Keras/2.2.4-foss-2019a-Python-3.7.2
module load tools/Horovod/0.16.3-foss-2019a-Python-3.7.2

## Create tests directory and clone the TF benchmarks inside
TRAIN_SCRIPT=/home/users/yakl/parallelgrid/project/lu.mics.pagc.project/scripts/horovod/keras_imagenet_resnet50.py
TRAIN_DIR=$SCRATCH/imagenet/train/
VAL_DIR=$SCRATCH/imagenet/val/
EPOCHS=30
JOBNAME=MHVTFG-H2N1G2
GPUS=2
HOSTNAMES=$(scontrol show hostnames | sed "s/$/:${GPUS}/g" | paste -sd ,)
LOG_DIR=/home/users/yakl/parallelgrid/project/lu.mics.pagc.project/scripts/horovod/runs/horovod/$JOBNAME
if [ ! -d $LOGDIR ] ;
then
	mkdir LOG_DIR
fi
## Horovod execution
mpirun -np $SLURM_NTASKS \
            -H $HOSTNAMES \
        python $TRAIN_SCRIPT \
              --train-dir $TRAIN_DIR \
              --val-dir $VAL_DIR \
              --log-dir $LOG_DIR \
              --epochs $EPOCHS \
              --batch-size 100 
