#!/bin/bash -l
#SBATCH -J HorovodPytorchTFGPU
#SBATCH -o %x_%j.out
#SBATCH -N 2
#SBATCH --ntasks-per-node=2
#SBATCH --gpus-per-node=2
#SBATCH -t 0:15:0
#SBATCH -p gpu
#SBATCH --reservation=parallelcomputinggpu

GPUs_NODE=2
module purge
module restore latest-horovod-pyt-ml

source ~/venv/horovod-pytorch/bin/activate

## Only needed during HPC School 2019.06
#module load swenv/default-env/devel

## Part of 2019 software environment
#module load tools/Horovod/0.16.3-foss-2019a-Python-3.7.2

#source ~/venv/pyvi/bin/activate

## Create tests directory and clone the TF benchmarks inside
HOMEDIR=/home/users/yakl/parallelgrid/project/lu.mics.pagc.project
WORKDIR=$HOMEDIR/scripts/horovod-pytorch
TRAIN_SCRIPT=$WORKDIR/pytorch_imagenet_resnet50.py
TRAIN_DIR=$SCRATCH/imagenet/train/
VAL_DIR=$SCRATCH/imagenet/val/
TIMENOW=$(date +%T)
LOG_DIR=$WORKDIR/runs/${TIMENOW}/N${SLURM_NNODES}/G${GPUs_NODE}
if [ ! -d $LOG_DIR ] ;
then 
	mkdir -p $LOG_DIR 
fi
LOGFILE=${SLURM_JOB_NAME}-${SLURM_JOB_ID}.out
EPOCHS=10

START_T=$(date +%s)
echo "start : $START_T"

GPUs=$(expr ${SLURM_NNODES} \* $GPUs_NODE)
echo "Parameters:"
echo "NTASKS : ${SLURM_NTASKS} (workers)."
echo "NNODES : ${SLURM_NNODES}"
echo "task(x Nodes) : ${SLURM_TASKS_PER_NODE}"
echo "GPU per nodes : ${SLURM_STEP_GPUS}"
echo "GPUs total : $GPUs"
echo "EPOCHS : $EPOCHS"
echo "Training DataSet : $IMGNET_DIR"

## Horovod execution
horovodrun -np ${SLURM_NTASKS}\
        python $TRAIN_SCRIPT \
              --train-dir $TRAIN_DIR \
              --val-dir $VAL_DIR \
              --log-dir $LOG_DIR \
              --epochs $EPOCHS \
              --batch-size 100

