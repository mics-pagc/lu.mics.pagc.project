CURDIR="$(pwd)"
LAUNCHER_TEMPLATE="$CURDIR/../launcher.sh"
NODELIST=(2 4 8)
GPUPN=(1 2 4)
JOBHR=(2)

for N in ${NODELIST[@]};
do
	for G in ${GPUPN[@]};
	do
		for H in ${JOBHR[@]};
		do
		LAUNCHERDIR=$CURDIR/H$H/N$N/G$G
		echo "creating dir $LAUNCHERDIR"
		mkdir -p $LAUNCHERDIR
		echo "creating launch file"
		cp ${LAUNCHER_TEMPLATE} $LAUNCHERDIR
		echo "updating params"
		sed -i "s/#SBATCH -J HorovodPytorchTFGPU/#SBATCH -J HVDPT-H${H}N${N}G${G}/g" $LAUNCHERDIR/launcher.sh
		sed -i "s/#SBATCH -N 2/#SBATCH -N $N/g" $LAUNCHERDIR/launcher.sh  
		sed -i "s/#SBATCH --ntasks-per-node=2/#SBATCH --ntasks-per-node=$G/g" $LAUNCHERDIR/launcher.sh
		sed -i "s/#SBATCH --gpus-per-node=2/#SBATCH --gpus-per-node=$G/g" $LAUNCHERDIR/launcher.sh
		sed -i "s/#SBATCH -t 0:15:0/#SBATCH -t 0${H}:00:0/g" $LAUNCHERDIR/launcher.sh
		sed -i "s/GPUs_NODE=2/GPUs_NODE=$G/g" $LAUNCHERDIR/launcher.sh
		echo "adding permission to new launcher"
		chmod +x $LAUNCHERDIR/launcher.sh
		echo "submitting launcher to sbatch $LAUNCHERDIR/launcher.sh"
		sbatch $LAUNCHERDIR/launcher.sh
		done
	done
done
