# Steps to setup the virtual env used by `launcher.sh`

- Connect to a single interactive node : `si`
load the following modules
module load swenv/default-env/v1.2-20191021-production 
module load toolchain/foss/2019a 
module load system/CUDA numlib/cuDNN
module load lang/Python/3.7.2-GCCcore-8.2.0 

- save the module configuration as latest-horovod-pyt-ml
module save latest-horovod-pyt-ml

- if you don't already have one create a directory for python virtualenvironments
    ```mkdir ~/venv/ && cd ~/venv```

- create virtual env dir and subdir : 
  ```virtualenv horovod-pytorch```

- download dependencies : `pip3 install -r lu.mics.pagc.project/scripts/horovod-pytorch/requirements.txt`
