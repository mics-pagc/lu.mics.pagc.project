#!/bin/bash -l
#SBATCH -J PyTorchGPU
#SBATCH -o %x_%j.out
#SBATCH -N 1 
#SBATCH -n 1
#SBATCH --ntasks-per-node=1
#SBATCH --gpus-per-node=4
#SBATCH --gpus-per-task=4
#SBATCH -t 2:0:0
#SBATCH -p gpu
GPUs_NODE=4
module purge
module load swenv/default-env/v1.1-20180716-production
module load lang/Python/3.6.4-foss-2018a
module load system/CUDA/9.1.85
source ~/venv/pyvi/bin/activate

#MASTER_HOST=$(scontrol show hostname ${SLURM_NODELIST} | head -n 1)

TRAIN_SCRIPT=scripts/pytorch/pytorch_imgnet.py
DISTRIBUTED_BACKEND="nccl"
LOG_DIR=runs/pytorch/${SLURM_NNODES}
LOGFILE=${SLURM_JOB_NAME}-${SLURM_JOB_ID}-${SLURMD_NODENAME}.out
IMGNET_DIR=${SCRATCH}/imagenet
EPOCHS=30
BATCH_SIZE=100
SHARED_FILE="$SCRATCH/sharedfile-$SLURM_JOB_ID"
DIST_FILE="file://$SHARED_FILE"
DIST_URL="tcp://${SLURM_LAUNCH_NODE_IPADDR}:${SLURM_STEP_LAUNCHER_PORT}"
DIST_ENV="env://"
DATA_WORKERS=$((4*$GPUs_NODE))
touch $SHARED_FILE

export MASTER_PORT="7979"
export MASTER_ADDR="$(scontrol show hostname ${SLURM_NODELIST} | head -n 1)"
export WORLD_SIZE=${SLURM_NTASKS}
export RANK=0

[[ ! -d $LOG_DIR ]] && mkdir -p $LOG_DIR 
START_T=$(date +%s)
echo "start : $START_T"

GPUs=$((${SLURM_NNODES}*$GPUs_NODE))
echo "Parameters:"
echo "NTASKS : ${SLURM_NTASKS} (workers)."
echo "NNODES : ${SLURM_NNODES}"
echo "task(x Nodes) : ${SLURM_TASKS_PER_NODE}"
echo "GPU per nodes : $GPUs_NODE"
echo "GPUs total : $GPUs"
echo "EPOCHS : $EPOCHS"
echo "Training DataSet : $IMGNET_DIR"
#echo "Master node ${MASTER_ADDR}:${MASTER_PORT}"
echo "Num data loading workers $DATA_WORKERS"
T=$((${SLURM_NTASKS}-1))

for i in `seq 0 $T`; 
do
    export RANK=$i
    srun -N1 -n1 \
       python3 $TRAIN_SCRIPT \
        --epochs $EPOCHS \
        --arch resnet50 \
        --dist-url $DIST_FILE \
        --dist-backend $DISTRIBUTED_BACKEND \
        --multiprocessing-distributed \
        --world-size ${SLURM_NTASKS} \
        --rank $RANK \
        --workers $DATA_WORKERS \
        --batch-size $BATCH_SIZE \
        $IMGNET_DIR > $LOG_DIR/${SLURM_JOB_NAME}-${SLURM_JOB_ID}-W$i.out &
done
wait
END_T=$(date +%s)
echo "end: $END_T"
echo "Total time $(($END_T-$START_T)) "
rm -f $SHARED_FILE
