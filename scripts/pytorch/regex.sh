#!/bin/bash
#File input for metric computation
if [ "$1" == "-h" ]||[ $# -lt 4 ]; then
  echo "Usage: `basename $0` *inputfile *number_of_epochs *number_of_nodes *number_of_batches"
  exit 0
fi

file=$1

#Epoch number of a particular configuration
nepochs=$2

#Number of nodes
nnodes=$3

#Number of batches per nodes
nbatches=$4

#batch size
batch=100

#best accuracy
acc=$5

gpus=4;

total_time_sum=0;
count=0
for e in `seq 0 $(($nepochs-1))`;
do
total_time_epoch=0;
sum_epoch_time=0;
#output collection of times for a given epoch
times=$(cat $file | grep -oP 'Epoch: \['$e'\].*Time\s*\d{1,2}\.\d{3}' | grep -oP '\d{1,2}\.\d{3}');
    for t in $times
    do
        sum_epoch_time=$(echo $sum_epoch_time+$t | bc -l);
    done
total_time_epoch=$(echo "scale=3;$sum_epoch_time/$gpus" | bc -l);
total_time_sum=$(echo $total_time_sum+$total_time_epoch | bc -l)
done
average_time_epochs=$(echo "scale=3;$total_time_sum/$nepochs" | bc -l)
total_images=$(echo $nbatches*100 | bc -l)
images_sec=$(echo "scale=3;$total_images/$average_time_epochs*$nnodes" | bc -l)
echo "$nnodes $average_time_epochs $total_images $images_sec"
    
