# Steps to setup the virtual env used by `launcher.sh`

- Connect to a single interactive node : `si`
- load python 3.6.5 module : 
    ```module load lang/Python/3.6.4-foss-2018a```
- create virtual env dir and subdir : 
    ```mkdir ~/venv/pyvi && cd ~/venv```
- initilise virtual env : `virtualenv pyvi`
- download dependencies : `pip3 install -r lu.mics.pagc.project/scripts/pytorch/requirements.txt`
- After the whole process, the output of `pip3 freeze` should be : \
    `` cffi==1.13.2
cloudpickle==1.2.2
horovod==0.18.2
numpy==1.18.1
Pillow==6.2.2
protobuf==3.11.2
psutil==5.6.7
pycparser==2.19
PyYAML==5.3
six==1.13.0
tensorboardX==2.0
torch==1.3.1
torchvision==0.4.2
tqdm==4.41.1 ``
